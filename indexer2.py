import argparse
import os
import re
import math
from collections import Counter

# Função para contar palavras em um arquivo
def contar_palavras_em_arquivo(caminho_arquivo):
    contagem_palavras = Counter()  # Cria um contador de palavras vazio
    with open(caminho_arquivo, 'r') as arquivo:
        texto = arquivo.read().lower()  # Lê o conteúdo do arquivo e converte para letras minúsculas
        palavras = re.findall(r'\b\w+\b', texto)  # Usa expressão regular para encontrar palavras
        palavras = [palavra for palavra in palavras if len(palavra) >= 2]  # Remove palavras curtas
        contagem_palavras.update(palavras)  # Conta a frequência das palavras
    return contagem_palavras

# Função para calcular o TF-IDF de um termo em um documento
def tfidf(termo, contagem_palavras, indice_documentos):
    tf = contagem_palavras[termo] / sum(contagem_palavras.values())  # Calcula o TF (Term Frequency)
    idf = math.log(len(indice_documentos) / (1 + sum(1 for doc in indice_documentos.values() if termo in doc)))  # Calcula o IDF (Inverse Document Frequency)
    return tf * idf  # Combina TF e IDF para obter o valor TF-IDF

# Função para indexar um documento
def indexar_documento(caminho_arquivo, indice_documentos):
    contagem_palavras = contar_palavras_em_arquivo(caminho_arquivo)  # Conta as palavras no arquivo
    indice_documentos[caminho_arquivo] = contagem_palavras  # Armazena a contagem no índice

# Função para buscar documentos relevantes para um termo de busca
def buscar(termo, indice_documentos):
    resultados = {}
    for documento, contagem_palavras in indice_documentos.items():
        resultados[documento] = tfidf(termo, contagem_palavras, indice_documentos)  # Calcula o TF-IDF para cada documento
    resultados_ordenados = dict(sorted(resultados.items(), key=lambda item: item[1], reverse=True))  # Ordena os resultados por relevância
    return resultados_ordenados

# Função para exibir o menu de operações
def mostrar_menu():
    print()
    print("Escolha uma operação:")
    print("1. Exibir as N palavras mais frequentes em um arquivo (--freq)")
    print("2. Exibir a contagem de uma palavra específica em um arquivo (--freq-word)")
    print("3. Exibir documentos relevantes para um termo de busca (--search)")
    print("4. Sair")

# Função principal do programa
def main():
    indice_documentos = {}  # Dicionário para armazenar as contagens de palavras de documentos
    
    while True:  # Loop principal para interação com o usuário
        mostrar_menu()  # Mostra o menu de operações
        print()
        escolha = input("Digite o número da operação desejada: ")  # Solicita a escolha do usuário
        
        if escolha == '1':
            n = int(input("Digite o número de palavras mais frequentes: "))  # Solicita o número de palavras mais frequentes
            documento = input("Digite o nome do arquivo: ")  # Solicita o nome do arquivo
            indexar_documento(documento, indice_documentos)  # Indexa o documento
            contagem_palavras = indice_documentos[documento]  # Obtém a contagem de palavras
            mais_comuns = contagem_palavras.most_common(n)  # Obtém as palavras mais frequentes
            print()
            print("Palavra(s) frequente(s):")
            for palavra, contagem in mais_comuns:
                print(f"{palavra}: {contagem}")  # Exibe as palavras mais frequentes e suas contagens
        elif escolha == '2':
            palavra = input("Digite a palavra: ")  # Solicita a palavra
            documento = input("Digite o nome do arquivo: ")  # Solicita o nome do arquivo
            indexar_documento(documento, indice_documentos)  # Indexa o documento
            contagem_palavras = indice_documentos[documento]  # Obtém a contagem de palavras
            contagem = contagem_palavras.get(palavra, 0)  # Obtém a contagem da palavra
            print()
            print(f"A palavra '{palavra}' aparece {contagem} vezes em {documento}")  # Exibe a contagem da palavra
        elif escolha == '3':
            termo = input("Digite o termo de busca: ")  # Solicita o termo de busca
            arquivos_para_buscar = input("Digite os nomes dos arquivos separados por espaço: ").split()  # Solicita os nomes dos arquivos
            for arquivo in arquivos_para_buscar:
                indexar_documento(arquivo, indice_documentos)  # Indexa os documentos para busca
            resultados_busca = buscar(termo, indice_documentos)  # Realiza a busca
            print()
            print(f"Documentos mais relevantes para '{termo}':")
            for documento, relevancia in resultados_busca.items():
                print(f"{documento}: TF-IDF = {relevancia}")  # Exibe os documentos relevantes
        elif escolha == '4':
            break  # Sai do programa se o usuário escolher a opção 4
        else:
            print("Opção inválida. Por favor, escolha uma operação válida.")  # Mensagem de erro para escolhas inválidas

if __name__ == "__main__":
    main()  # Inicia o programa
