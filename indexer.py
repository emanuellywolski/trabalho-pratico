import argparse
import os
import re
import math
from collections import Counter

# Função para contar palavras em um arquivo
def count_words_in_file(file_path):
    word_count = Counter()
    with open(file_path, 'r') as file:
        text = file.read().lower()
        words = re.findall(r'\b\w+\b', text)
        words = [word for word in words if len(word) >= 2]
        word_count.update(words)
    return word_count

# Função para calcular o TF-IDF de um termo em um documento
def tfidf(term, word_count, document_index):
    tf = word_count[term] / sum(word_count.values())
    idf = math.log(len(document_index) / (1 + sum(1 for doc in document_index.values() if term in doc)))
    return tf * idf

# Função para indexar um documento
def index_document(file_path, document_index):
    word_count = count_words_in_file(file_path)
    document_index[file_path] = word_count

# Função para buscar documentos relevantes para um termo de busca
def search(term, document_index):
    results = {}
    for document, word_count in document_index.items():
        results[document] = tfidf(term, word_count, document_index)
    sorted_results = dict(sorted(results.items(), key=lambda item: item[1], reverse=True))
    return sorted_results

def show_menu():
    print()
    print("Escolha uma operação:")
    print("1. Exibir as N palavras mais frequentes em um arquivo")
    print("2. Exibir a contagem de uma palavra específica em um arquivo")
    print("3. Exibir documentos relevantes para um termo de busca")
    print("4. Sair")

def main():
    document_index = {}
    
    while True:
        show_menu()
        print()
        choice = input("Digite o número da operação desejada: ")
        
        if choice == '1':
            n = int(input("Digite o número de palavras mais frequentes: "))
            document = input("Digite o nome do arquivo: ")
            index_document(document, document_index)
            word_count = document_index[document]
            most_common = word_count.most_common(n)
            print()
            print("Palavra(s) frequente(s):")
            for word, count in most_common:
                print(f"{word}: {count}")
        elif choice == '2':
            word = input("Digite a palavra: ")
            document = input("Digite o nome do arquivo: ")
            index_document(document, document_index)
            word_count = document_index[document]
            count = word_count.get(word, 0)
            print()
            print(f"A palavra '{word}' aparece {count} vezes em {document}")
        elif choice == '3':
            term = input("Digite o termo de busca: ")
            files_to_search = input("Digite os nomes dos arquivos separados por espaço: ").split()
            for file in files_to_search:
                index_document(file, document_index)
            search_results = search(term, document_index)
            print()
            print(f"Documentos mais relevantes para '{term}':")
            for document, relevance in search_results.items():
                print(f"{document}: TF-IDF = {relevance}")
        elif choice == '4':
            break
        else:
            print("Opção inválida. Por favor, escolha uma operação válida.")

if __name__ == "__main__":
    main()
